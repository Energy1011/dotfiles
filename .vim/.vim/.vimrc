set nocompatible
source $VIMRUNTIME/vimrc_example.vim
source $VIMRUNTIME/mswin.vim
"behave mswin

"Personal Settings.
execute pathogen#infect()
filetype plugin indent on
syntax on

" Set my leader key
let mapleader=","

"Sets 
set nu
set ruler		" show the cursor position all the time
set laststatus=2
set nocompatible
set hidden
set tabstop=4
set shiftwidth=4

colorscheme gotham 

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>


" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'
nmap <leader>t :enew<cr>
nmap <leader>ne :NERDTree<cr>

" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>T :enew<cr>

"http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
" Move to the next buffer
nmap <leader>l :bnext<CR>
nnoremap <C-tab>   :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>
nnoremap <C-S-tab> :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" Easy bindings for its various modes
nmap <leader>bb :CtrlPBuffer<cr>
nmap <leader>bm :CtrlPMixed<cr>
nmap <leader>bs :CtrlPMRU<cr>

" clipboard section
	" bindings to copy and paste to clipboard
	" * is for primary selected clipboard
	" + is for normal clipbaord 
	noremap <Leader>y "*y
	noremap <Leader>p "*p
	noremap <Leader>Y "+y
	noremap <Leader>P "+p

