# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block, everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:/usr/local/bin:$PATH

# Path to your oh-my-zsh installation.
export ZSH="/home/energy/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
#ZSH_THEME="robbyrussell"
ZSH_THEME=powerlevel10k/powerlevel10k

# Set list of themes to pick from when loading at random
# Setting this variable when ZSH_THEME=random will cause zsh to load
# a theme from this variable instead of looking in ~/.oh-my-zsh/themes/
# If set to an empty array, this variable will have no effect.
# ZSH_THEME_RANDOM_CANDIDATES=( "robbyrussell" "agnoster" )

# Uncomment the following line to use case-sensitive completion.
# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion.
# Case-sensitive completion must be off. _ and - will be interchangeable.
# HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to automatically update without prompting.
# DISABLE_UPDATE_PROMPT="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line if pasting URLs and other text is messed up.
# DISABLE_MAGIC_FUNCTIONS=true

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files
# under VCS as dirty. This makes repository status check for large repositories
# much, much faster.
# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
# stamp shown in the history command output.
# You can set one of the optional three formats:
# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
# or set a custom format using the strftime function format specifications,
# see 'man strftime' for details.
# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load?
# Standard plugins can be found in ~/.oh-my-zsh/plugins/*
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(git vi-mode fzf tmux)
source $ZSH/oh-my-zsh.sh

# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

export HISTFILESIZE=5000;
#export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export HISTSIZE=5000;

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion


# ALIAS
alias rm='rm -I'
#alias cat='bat --paging=never'
alias chrome='chrome -incognito'
alias rm='rm -i'
alias mv='mv -i'
alias cp='cp'
alias mocp='mocp -T default'
alias node='nodejs'
alias ran='ranger'
# Print my public IP
alias myip='curl ipinfo.io/ip'

#git aliases
alias gitl='git log | less'
alias gits='omgit'
alias gitp='git push'
alias gitd='git diff | less'

#other aliases
alias setmytime='ntpdate 0.pool.ntp.org'
alias gp='. /usr/bin/gp'
alias meteor='meteor --allow-superuser'
# take a screenshot in five seconds: $ screenshot 5
alias screenshot_count='scrot -c -d $1'
alias screenshot_area='TEMP_F="%Y-%m-%d-%s.png";scrot -s $TEMP_F -e "mv $TEMP_F ~/Pictures/"'
alias vim='sp && vim'
alias vim='sp && vim'
alias google-chrome='google-chrome --no-sandbox'

#Docker aliases
alias laradockstop='sudo docker-compose stop'
alias laradockdown='sudo docker-compose down'
alias laradockquit='laradockstop && laradockdown'
alias laradockbash='sudo docker-compose exec --user=laradock workspace bash'
alias laradockroot='sudo docker-compose exec --user=root workspace bash'
alias laradockscript='sudo docker-compose exec workspace'
alias laradockup='sudo docker-compose up -d'
alias laraclearlogs='if [ -f storage/logs/laravel.log ]; then rm -R storage/logs/*.log; echo "Found laravel.log"; else echo "laravel.log not found."; fi'
alias laraopenlog='atom storage/logs/laravel.log'
alias laradockips='sudo docker inspect -f "{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" $(sudo docker ps -aq)'
alias editbashrc='vim ~/.bashrc'
alias openc='code $(fzf -m)'
alias ssh_init_agent_on_demand='eval $(ssh-agent) && ssh-add'
alias ssh_drop_keys='ssh-add -D'

# k8s
alias k='kubectl'

#Function to copy/sync files in a project to crudvel git project
function crudvel-cp(){
  #TODO improve to generic mode
  rsync -avzh /home/$USER/projects/$1/vendor/benomas/crudvel/src/ /home/$USER/projects/crudvel/src/
}

#Function to send a msg with marvin
function marvinmsg(){
  bash /usr/bin/marvinmsg $1	
}

# Function to combine bat with git diff
function batdiff() {
  git diff --name-only --relative --diff-filter=d | xargs bat --diff
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# bat can be used as a colorizing pager for man, by setting the MANPAGER environment variable: 
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
