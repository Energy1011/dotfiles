#!/bin/bash 

# Script to backup dotfiles 
# Copyright (C) 2016 4L3J4NDR0 4N4Y4 (energy1011[4t]gmail[d0t]com)

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

#vim
function bk_vim(){
	echo "[i] Copying vim files";
	cp -rf ~/.vim/ .
	cp ~/.vimrc .
}

#atom
function bk_atom(){
	echo "[i] Copying atom files";
	cp ~/.atom/config.cson .atom/
	cp ~/.atom/keymap.cson .atom/
}

#bashrc
function bk_bashrc(){
	echo "[i] Copying .bashrc";
	cp ~/.bashrc .
}

#zshrc
function bk_zshrc(){
	echo "[i] Copying .zshrc";
	cp ~/.zshrc .
}

#p10k oh-my-zsh theme
function bk_p10k(){
	echo "[i] Copying .p10k.zsh theme";
	cp ~/.p10k.zsh .
}

#i3
function bk_i3(){
	echo "[i] Copying i3 config";
	cp -rf ~/.config/i3 .config/
	cp -rf ~/.config/i3status .config/i3status
}

#tmux
function bk_tmux(){
	echo "[i] Copying tmux config";
	cp ~/.tmux.conf .tmux.conf
}

#Main function
function main(){
	echo "[i] Backup dotfiles";

	echo -e "[?] Backup ~/.bashrc (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_bashrc
	fi

	echo -e "[?] Backup ~/.zshrc (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_zshrc
	fi

	echo -e "[?] Backup ~/.p10k.zsh (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
                bk_p10k
	fi
	echo -e "[?] Backup ~/.vimrc and vim files (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_vim
	fi

#	echo -e "[?] Backup ~/.atom files (y/n)";
#	read -n1 option
#	if [ "$option" == "y" ]; then
#		bk_atom
#	fi


	echo -e "[?] Backup ~/.config/i3 (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_i3
	fi

	echo -e "[?] Backup ~/.tmux.conf (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		bk_tmux
	fi

#	echo -e "[?] Backup ~/.config/ranger (y/n)";
#	read -n1 option
#	if [ "$option" == "y" ]; then
#		bk_ranger
#	fi
#
}

#Call main function
main
#Call git 
git -v >& /dev/null
if [ $? -ne 0 ]; then
	#Git is installed 
	#Call git diff 
	echo -e "[?] Show git diff (y/n)";
	read -n1 option
	if [ "$option" == "y" ]; then
		git diff	
	fi

fi

echo -e "\nScript ends";
