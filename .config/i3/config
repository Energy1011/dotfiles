#i3 config
set $mod Mod4
font pango: Noto Sans 8

# common apps keybinds
bindsym Print exec gnome-screenshot

# exit i3
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'Really, exit?' -b 'Yes' 'i3-msg exit'"

# change container layout split
bindsym $mod+Shift+s layout toggle split

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec gnome-terminal

# kill focused window
bindsym $mod+Shift+q kill

# change focus
#bindsym $mod+j focus left
#bindsym $mod+k focus down
#bindsym $mod+l focus up
#bindsym $mod+semicolon focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
#bindsym $mod+h split h

# split in vertical orientation
#bindsym $mod+v split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# aditional screens move
bindsym $mod+Shift+y move container to workspace 1
bindsym $mod+Shift+i move container to workspace 2
bindsym $mod+Shift+o move container to workspace 3
bindsym $mod+Shift+u move container to workspace 4

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# lock screen
bindsym $mod+Escape exec "i3lock"

# start dmenu (a program launcher)
bindsym $mod+Shift+d exec i3-dmenu-desktop --dmenu="dmenu -i -fn 'Noto Sans:size=8'"

bindsym $mod+d exec "~/.config/rofi/bin/launcher_misc"
bindsym $mod+m exec "rofimoji"

#old rofi
#bindsym $mod+d exec "rofi -lines 12 -padding 18 -width 60 -location 0 -show drun -sidebar-mode -columns 3 -font 'Noto Sans 8'"
#bindsym $mod+Tab exec "rofi -lines 12 -padding 18 -width 60 -location 0 -show window -sidebar-mode -columns 3 -font 'Noto Sans 8'"


# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym h resize shrink width 10 px or 10 ppt
        bindsym j resize grow height 10 px or 10 ppt
        bindsym k resize shrink height 10 px or 10 ppt
        bindsym l resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Esc
        bindsym Return mode "default"
        bindsym Esc mode "default"
}

bindsym $mod+r mode "resize"


# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
  status_command i3status
  font pango:DejaVu Sans Mono 20
  colors {
    statusline #FFFFFF
    background #000000
    focused_workspace #282828 #282828 #FFFFFF
    inactive_workspace #333333 #333333 #AAAAAA
  }
}


exec gnome-settings-daemon
#exec --no-startup-id xcompmgr &> /dev/null
exec setxkbmap -layout latam
#gaps inner 20
exec --no-startup-id feh --bg-fill ~/Downloads/bgs/bg.png
exec --no-startup-id xsettingsd &
exec --no-startup-id compton -b
exec --no-startup-id copyq # To start copyq
exec --no-startup-id nm-applet
exec firefox
exec gnome-terminal 

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# split in horizontal orientation
bindsym $mod+semicolon split h

# split in vertical orientation
bindsym $mod+v split v

client.unfocused #333333 #1B1C1C #FFFFFF #333333
client.focused_inactive #333333 #333333 #FFFFFF #000000
#client.focused #7C2106 #7C2106 #FFFFFF #000000
client.focused #000000 #6D0300 #FFFFFF #000000

#--- Pulse Audio controls------------------------------------
bindsym XF86AudioRaiseVolume exec --no-startup-id pactl set-sink-volume bluez_sink.38_F3_2E_DD_E5_03.a2dp_sink +5% || pactl set-sink-volume 1 +5% || pactl set-sink-volume 0 +5% #increase sound volume
bindsym XF86AudioLowerVolume exec --no-startup-id pactl set-sink-volume bluez_sink.38_F3_2E_DD_E5_03.a2dp_sink -5% || pactl set-sink-volume 1 -5%  || pactl set-sing-volume 0 -5% #decrease sound volume
bindsym XF86AudioMute exec --no-startup-id pactl set-sink-mute 1 toggle || pactl set-sink-mute 0 toggle # mute sound
#----------------------------------------------------------------------

# # Sreen brightness controls
#bindsym XF86MonBrightnessUp exec xbacklight -inc 20 # increase screen brightness
#bindsym XF86MonBrightnessDown exec xbacklight -dec 20 # decrease screen brightness
bindsym XF86MonBrightnessUp exec ~/.config/i3/brightnessControl.sh + 10
bindsym XF86MonBrightnessDown exec ~/.config/i3/brightnessControl.sh - 10

#
# # Touchpad controls
## bindsym XF86TouchpadToggle exec /some/path/toggletouchpad.sh # toggle touchpad
#
# # Media player controls
bindsym XF86AudioPlay exec playerctl play
bindsym XF86AudioPause exec playerctl pause
bindsym XF86AudioNext exec playerctl next
bindsym XF86AudioPrev exec playerctl previous

for_window [class="^.*"] border pixel 3 

# prevent i3 losing focus with mpv player while Im using mpsyt
no_focus [class="mpv"]

bindsym $mod+z sticky toggle

## mode for randr
# source: https://christopherdecoster.com/posts/i3-wm/
set $displayMode "Set display mode [w]ork [l]aptop [h]ome"
#bindsym $mod+x mode $displayMode
mode $displayMode {
    bindsym w exec "\
                ~/.config/i3/scripts/work-displays.sh && \
                ~/.config/i3/scripts/dpi-96.sh"; \
                mode "default"
    bindsym h exec "\
                ~/homescreenlayout-double-h.sh"; \
                mode "default"
    bindsym l exec "\
                ~/.config/i3/scripts/laptop-display.sh && \
                ~/.config/i3/scripts/dpi-192.sh"; \
                mode "default"

    bindsym Return mode "default"
    bindsym Esc mode "default"
}
exec setxkbmap -layout es


