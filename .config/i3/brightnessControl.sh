#!/bin/bash
actual_brightness=$( cat /sys/class/backlight/intel_backlight/actual_brightness )
val=$(expr $actual_brightness $1 $2)
if [ $val -lt 931 ] && [ $val -gt -1 ]; then
  echo $val > /sys/class/backlight/intel_backlight/brightness
fi
