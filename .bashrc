# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
[ -z "$PS1" ] && return

# don't put duplicate lines in the history. See bash(1) for more options
# don't overwrite GNU Midnight Commander's setting of `ignorespace'.
HISTCONTROL=$HISTCONTROL${HISTCONTROL+:}ignoredups
# ... or force ignoredups and ignorespace
HISTCONTROL=ignoreboth

# append to the history file, don't overwrite it
shopt -s histappend

# for setting history length see HISTSIZE and HISTFILESIZE in bash(1)

# check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize

# make less more friendly for non-text input files, see lesspipe(1)
#[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"

# set variable identifying the chroot you work in (used in the prompt below)
if [ -z "$debian_chroot" ] && [ -r /etc/debian_chroot ]; then
    debian_chroot=$(cat /etc/debian_chroot)
fi

# set a fancy prompt (non-color, unless we know we "want" color)
case "$TERM" in
    xterm-color) color_prompt=yes;;
esac

# uncomment for a colored prompt, if the terminal has the capability; turned
# off by default to not distract the user: the focus in a terminal window
# should be on the output of commands, not on the prompt
#force_color_prompt=yes

if [ -n "$force_color_prompt" ]; then
    if [ -x /usr/bin/tput ] && tput setaf 1 >&/dev/null; then
	# We have color support; assume it's compliant with Ecma-48
	# (ISO/IEC-6429). (Lack of such support is extremely rare, and such
	# a case would tend to support setf rather than setaf.)
	color_prompt=yes
    else
	color_prompt=
    fi
fi

if [ "$color_prompt" = yes ]; then
    PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
else
    PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
fi

#Prompt colors
SYMBOL=`echo -e "\u2620\033[01;32m $USER\033[00m:"`;
PS1='$SYMBOL\w$(__git_ps1 "[[%s]]")->'
PS1='\[\e[1;32m\]\u@\h:\w${text}$\[\e[m\] '
PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]$(__git_ps1 "[[%s]]")\$ '
unset color_prompt force_color_prompt
# If this is an xterm set the title to user@host:dir
case "$TERM" in
xterm*|rxvt*)
    PS1="\[\e]0;${debian_chroot:+($debian_chroot)}\u@\h: \w\a\]$PS1"
    ;;
*)
    ;;
esac

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias py='python'
fi

rojo='e\[1;34m'
# some more ls aliases
alias lsa='ls -Al'
alias lsc='ls *.c'
alias su='
echo "  ______  _____   _____  _______";
echo " |_____/ |     | |     |    |";
echo " |    \_ |_____| |_____|    |";
echo -e "            ${rojo} $USER";
su;'
alias chrome='chrome -incognito'
alias rm='rm -i'
#alias cat='bat'
alias mv='mv -i'
alias cp='cp'
alias mocp='mocp -T default'
alias node='nodejs'
alias ran='ranger'
# Print my public IP
alias myip='curl ipinfo.io/ip'
#alias l='ls -CF'

# Alias definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_aliases, instead of adding them here directly.
# See /usr/share/doc/bash-doc/examples in the bash-doc package.

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

export EXINIT='set nu ai'
#this is default java
#export JAVA_HOME='/usr/lib/jvm/default-java'
#this is for cordova
export JAVA_HOME='/usr/local/jdk1.8.0_151'
#PATH=$PATH:$JAVA_HOME:$JAVA_HOME/bin:$JAVA_HOME/jre/bin
export ANDROID_HOME='/root/Android/Sdk'
export PATH=$PATH:$JAVA_HOME:$ANDROID_HOME/tools:$ANDROID_HOME/platform-tools:$ANDROID_HOME/build-tools:$ANDROID_HOME/bin:/opt/android-studio/bin/
# fix bug for system libs when run android emulator
export ANDROID_EMULATOR_USE_SYSTEM_LIBS=1
export EDITOR='vi'

#grafico de debian
rojo='\e[1;31m'
NC='\e[0m'
 
HISTFILESIZE=5000;
HISTSIZE=5000;
#DISPLAY=:0
##export XAUTHORITY=/home/$USER/.Xauthority

export HISTTIMEFORMAT='[%F %T] '

if [ "$TERM" = "linux" ]; then
  /bin/echo -e "
  \e]P023598e
  \e]P11f9e00
  \e]P231a354
  \e]P3dca060
  \e]P43182bd
  \e]P5756bb1
  \e]P680b1d3
  \e]P7b7b8b9
  \e]P8737475
  \e]P900d300
  \e]PA31a354
  \e]PBdca060
  \e]PC3182bd
  \e]PD756bb1
  \e]PE80b1d3
  \e]PFfcfdfe
  "
  # get rid of artifacts
  clear
fi

# 256 colors tmux
export TERM=xterm-256color
alias tmux="TERM=screen-256color-bce tmux"

#Turning on xcompmgr 
#if pgrep xcompmgr &>/dev/null; then
    #echo "Turning xcompmgr ON"
#       xcompmgr -c -C -t-5 -l-5 -r4.2 -o.55 &2>/dev/null
#fi

function do_prompt {
	date=`date '+%D %T'`
	dir=`echo $PWD | sed "s@$HOME@~@"`
	echo "$date $dir"
	unset date dir
}
#. /usr/local/lib/python2.7/dist-packages/powerline/bindings/bash/powerline.sh

#git aliases
alias gitl='git log | less'
alias gits='omgit'
alias gitp='git push'
alias gitd='git diff | less'

#other aliases
alias setmytime='ntpdate 0.pool.ntp.org'
alias gp='. /usr/bin/gp'
alias meteor='meteor --allow-superuser'
# take a screenshot in five seconds: $ screenshot 5
alias screenshot_count='scrot -c -d $1'
alias screenshot_area='TEMP_F="%Y-%m-%d-%s.png";scrot -s $TEMP_F -e "mv $TEMP_F ~/Pictures/"'
alias vim='sp && vim'
alias vim='sp && vim'
alias google-chrome='google-chrome --no-sandbox'

#Docker aliases
alias laradockstop='sudo docker-compose stop'
alias laradockdown='sudo docker-compose down'
alias laradockquit='laradockstop && laradockdown'
alias laradockbash='sudo docker-compose exec --user=laradock workspace bash'
alias laradockroot='sudo docker-compose exec --user=root workspace bash'
alias laradockscript='sudo docker-compose exec workspace'
alias laradockup='sudo docker-compose up -d'
alias laraclearlogs='if [ -f storage/logs/laravel.log ]; then rm -R storage/logs/*.log; echo "Found laravel.log"; else echo "laravel.log not found."; fi'
alias laraopenlog='atom storage/logs/laravel.log'
alias laradockips='sudo docker inspect -f "{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}" $(sudo docker ps -aq)'
alias editbashrc='vim ~/.bashrc'
alias openc='code $(fzf -m)'

alias cv-cp-infys='cp -f -R /home/energy/projects/infys-universal/vendor/benomas/crudvel/src /home/energy/projects/crudvel'
if pgrep -x "i3" > /dev/null
then
  true
else
  if [ $SHLVL == "1" ];then
    startx /usr/bin/i3
  fi
fi

#git() {
#    #do things with parameters like $1 such as
#    # clone, pull, push
#    if [ "$1" = pull ]; then
#      echo "esto es un pull :)";
#    fi
#    # check for url type in clone
#    /usr/bin/git $@
#}
# Set vi mode bash
set -o vi
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
#rm ~/.local/share/recently-used.xbel
# create and cd directory
mkcd ()
{
  mkdir -p -- "$1" && cd -P -- "$1"
}

function openr() {
  if [ -f openr ]; then
    ./openr
  else 
    echo "No existe openr en el directorio actual";
  fi 
}

# Install Ruby Gems to ~/gems
export GEM_HOME="$HOME/gems"
export PATH="$HOME/gems/bin:$PATH"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
# bat can be used as a colorizing pager for man, by setting the MANPAGER environment variable:
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
# nvm
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
