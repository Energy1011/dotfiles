set nocompatible
"source $VIMRUNTIME/vimrc_example.vim
"source $VIMRUNTIME/mswin.vim
"behave mswin

"Personal Settings.
execute pathogen#infect()
filetype plugin indent on
syntax on
set encoding=utf8
set guifont=DroidSansMono\ Nerd\ Font\ 11

if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin('~/.vim/plugged')
" Plugins will go here in the middle.
Plug 'scrooloose/nerdtree', { 'on':  'NERDTreeToggle' }
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
call plug#end()

"" Esc maps
imap ;; <Esc>

" Set my leader key
let mapleader=","

" qq to record, Q to replay
nnoremap Q @q

"Sets
set nu
set ruler " show the cursor position all the time
set laststatus=2
set nocompatible
set hidden

" Set tab and spaces
"autocmd FileType * set tabstop=2|set shiftwidth=2|set noexpandtab
autocmd FileType * set tabstop=2 softtabstop=0 expandtab shiftwidth=2 smarttab autoindent
autocmd FileType python set tabstop=2 shiftwidth=2 expandtab autoindent

nmap <silent> <A-Up> :wincmd k<CR>
nmap <silent> <A-Down> :wincmd j<CR>
nmap <silent> <A-Left> :wincmd h<CR>
nmap <silent> <A-Right> :wincmd l<CR>

"Easier split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Enable the list of buffers
let g:airline#extensions#tabline#enabled = 1
" Show just the filename
let g:airline#extensions#tabline#fnamemod = ':t'

" To open a new empty buffer
" This replaces :tabnew which I used to bind to this mapping
nmap <leader>T :enew<cr>
nmap <leader>t :enew<cr>

"http://joshldavis.com/2014/04/05/vim-tab-madness-buffers-vs-tabs/
" Move to the next buffer
nmap <leader>l :bnext<CR>
nnoremap <C-tab>   :bnext<CR>

" Move to the previous buffer
nmap <leader>h :bprevious<CR>
nnoremap <C-S-tab> :bprevious<CR>

" Close the current buffer and move to the previous one
" This replicates the idea of closing a tab
nmap <leader>bq :bp <BAR> bd #<CR>
nmap <leader>bd :bd<CR>

" Show all open buffers and their status
nmap <leader>bl :ls<CR>

" Set Term colors
colorscheme monokai
hi Normal guibg=NONE ctermbg=NONE

" Backup files
set nobackup
set nowritebackup

"list chars, show tabs and trails
set list
set listchars=tab:→\ ,eol:¬,trail:⋅,extends:❯,precedes:❮

"Search current word in dir
map <F12> :grep -srnw --binary-files=without-match --exclude-dir=.git . -e <C-r><C-w> <bar> cwindow

" Search current word under cursor in actual file
nnoremap <F6> :/<C-r><C-w>

" Auto ident in a newline
imap <C-Return> <CR><CR><C-o>k<Tab>

" ===================== Clipboard
noremap <Leader>y "+y
noremap <Leader>p "+p
noremap <Leader>Y "*y
noremap <Leader>P "*p

" ========================== PLUGINS OPTIONS ==========================
" ====================== Markdown plugin
" Disable autofolding markdown
let g:vim_markdown_folding_disabled = 1

" ====================== Emmet plugin
" This is the tab completion for emmet html<tab>
imap <expr> <tab> emmet#expandAbbrIntelligent("\<tab>")

" ====================== Ctrl-p plugin
" Easy bindings for its various modes
"nmap <leader>bb :CtrlPBuffer<cr>
"nmap <leader>bm :CtrlPMixed<cr>
"nmap <leader>bs :CtrlPMRU<cr>
"nmap <leader>bt :CtrlPTag<cr>

" ====================== NerdTree
nmap <leader>ne :NERDTree<cr>
set relativenumber

" ====================== Tabularize
nnoremap <Leader>a :Tabularize /
vnoremap <Leader>a :Tabularize /

" Commenting blocks of code.
autocmd FileType c,cpp,java,scala,php,js,css let b:comment_leader = '// '
autocmd FileType sh,ruby,python   let b:comment_leader = '# '
autocmd FileType conf,fstab       let b:comment_leader = '# '
autocmd FileType tex              let b:comment_leader = '% '
autocmd FileType mail             let b:comment_leader = '> '
autocmd FileType vim              let b:comment_leader = '" '
noremap <silent> <leader>cc :<C-B>silent <C-E>s/^/<C-R>=escape(b:comment_leader,'\/')<CR>/<CR>:nohlsearch<CR>
noremap <silent> <leader>cu :<C-B>silent <C-E>s/^\V<C-R>=escape(b:comment_leader,'\/')<CR>//e<CR>:nohlsearch<CR>

" set sintax for vuejs files
au BufNewFile,BufRead,BufReadPost *.vue set syntax=html


"======================== Vimux plugin
" Prompt for a command to run
map <Leader>vp :VimuxPromptCommand<CR>
" Run last command executed by VimuxRunCommand
map <Leader>vl :VimuxRunLastCommand<CR>
" Inspect runner pane
map <Leader>vi :VimuxInspectRunner<CR>
" Zoom the tmux runner pane
map <Leader>vz :VimuxZoomRunner<CR>

"======================= FZF
nnoremap <Leader>p :FZF<CR>

nnoremap <silent><c-p> :FZF<CR>

function! RangeChooser()
    let temp = tempname()
    " The option "--choosefiles" was added in ranger 1.5.1. Use the next line
    " with ranger 1.4.2 through 1.5.0 instead.
    "exec 'silent !ranger --choosefile=' . shellescape(temp)
    if has("gui_running")
        exec 'silent !xterm -e ranger --choosefiles=' . shellescape(temp)
    else
        exec 'silent !ranger --choosefiles=' . shellescape(temp)
    endif
    if !filereadable(temp)
        redraw!
        " Nothing to read.
        return
    endif
    let names = readfile(temp)
    if empty(names)
        redraw!
        " Nothing to open.
        return
    endif
    " Edit the first item.
    exec 'edit ' . fnameescape(names[0])
    " Add any remaning items to the arg list/buffer list.
    for name in names[1:]
        exec 'argadd ' . fnameescape(name)
    endfor
    redraw!
endfunction
command! -bar RangerChooser call RangeChooser()

nnoremap <leader>r :<C-U>RangerChooser<CR>
nmap <F8> :TagbarToggle<CR>
